{ pkgs ? import <nixpkgs> {}, ...} :
pkgs.stdenv.mkDerivation {
    name = "elm-gol-env";
    buildInputs = [ pkgs.nodejs pkgs.elmPackages.elm pkgs.elmPackages.elm-format pkgs.gmp ];
    shellHook = ''
        set -e
        npm install elm-test --no-opts -s
        elm-package install -y
        elm-package install -y
        set +e
    '' + (if pkgs.system != "x86_64-linux" then "" else ''
        patchelf --set-interpreter ${pkgs.stdenv.glibc}/lib/ld-linux-x86-64.so.2 ./node_modules/elm-test/bin/elm-interface-to-json
        patchelf --set-rpath ${pkgs.gmp}/lib ./node_modules/elm-test/bin/elm-interface-to-json
    '');
}
