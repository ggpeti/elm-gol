module GameOfLifeTests exposing (..)

import Expect exposing (Expectation)
import Test exposing (..)
import GameOfLife
import Set


suite : Test
suite =
    describe "The GameOfLife"
        [ describe "initial model"
            [ test "is empty" <| always <| Expect.equal Set.empty GameOfLife.init
            ]
        , describe "step"
            [ test "is correct for empty" <|
                always <|
                    Expect.equal Set.empty (GameOfLife.step GameOfLife.init)
            , test "spawns new cell" <|
                always <|
                    Expect.true "Expected ( 1, 1 ) to be alive after an iteration (spawned)."
                        (Set.member ( 1, 1 ) <| GameOfLife.step (Set.fromList [ ( 1, 0 ), ( 0, 0 ), ( 0, 1 ) ]))
            , test "kills overcrowded cells" <|
                always <|
                    Expect.false "Expected ( 0, 0 ) to be dead after an iteration (overcrowded)."
                        (Set.member ( 0, 0 ) <| GameOfLife.step (Set.fromList [ ( 0, 0 ), ( 1, 0 ), ( 0, 1 ), ( -1, 0 ), ( 0, -1 ) ]))
            , test "keeps cells alive" <|
                always <|
                    Expect.true "Expected ( 0, 0 ) to stay alive after an iteration (kept alive)."
                        (Set.member ( 0, 0 ) <| GameOfLife.step (Set.fromList [ ( 1, 0 ), ( 0, 0 ), ( -1, 0 ) ]))
            ]
        ]
