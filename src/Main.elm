module Main exposing (..)

import GameOfLife
import GameOfLife.View
import Weather
import Weather.View
import Bootstrap.CDN
import Bootstrap.Button as Button
import Bootstrap.Navbar as Navbar
import Html
import Html.Attributes as Attrib
import Html.Events as Events


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


type alias Model =
    { gameOfLife : GameOfLife.Board
    , weather : Weather.Model
    , navbarState : Navbar.State
    , navState : Page
    }


type Page
    = GOL
    | Weather


type Msg
    = GOLMsg GameOfLife.Msg
    | WeatherMsg Weather.Msg
    | NavbarMsg Navbar.State
    | Navigate Page


subscriptions : Model -> Sub Msg
subscriptions model =
    Navbar.subscriptions model.navbarState NavbarMsg


init : ( Model, Cmd Msg )
init =
    let
        ( navbarState, navbarCmd ) =
            Navbar.initialState NavbarMsg

        model =
            { gameOfLife = GameOfLife.init
            , weather = Weather.init
            , navbarState = navbarState
            , navState = Weather
            }

        cmds =
            [ Weather.requestLocation |> (Cmd.map WeatherMsg)
            , navbarCmd
            ]
    in
        model ! cmds


view : Model -> Html.Html Msg
view model =
    Html.div []
        [ Bootstrap.CDN.stylesheet
        , viewNavbar model
        , Html.node "style" [] [ Html.text css ]
        , case model.navState of
            GOL ->
                model.gameOfLife |> GameOfLife.View.view 10 |> Html.map GOLMsg

            Weather ->
                model.weather |> Weather.View.view |> Html.map WeatherMsg
        ]


viewNavbar : Model -> Html.Html Msg
viewNavbar model =
    Navbar.config NavbarMsg
        |> Navbar.withAnimation
        |> Navbar.items
            [ (navbarItemLink model.navState Weather)
                [ Attrib.href "#"
                , Events.onClick (Navigate Weather)
                ]
                [ Html.text "Weather" ]
            , (navbarItemLink model.navState GOL)
                [ Attrib.href "#"
                , Events.onClick (Navigate GOL)
                ]
                [ Html.text "GoL" ]
            ]
        |> Navbar.view model.navbarState


navbarItemLink :
    a
    -> a
    -> List (Html.Attribute msg)
    -> List (Html.Html msg)
    -> Navbar.Item msg
navbarItemLink current target =
    if current == target then
        Navbar.itemLinkActive
    else
        Navbar.itemLink


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GOLMsg subMsg ->
            { model | gameOfLife = model.gameOfLife |> GameOfLife.update subMsg |> Tuple.first } ! []

        WeatherMsg subMsg ->
            let
                ( weather, cmd ) =
                    Weather.update subMsg model.weather
            in
                { model | weather = weather } ! [ cmd |> Cmd.map WeatherMsg ]

        NavbarMsg navbarState ->
            { model | navbarState = navbarState } ! []

        Navigate navState ->
            { model | navState = navState } ! []


css : String
css =
    """
@import url("https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900");
body {
  font-family: 'Montserrat', sans-serif;
  background: #112233;
}
"""
