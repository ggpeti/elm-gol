module GameOfLife exposing (..)

import Set exposing (Set)
import List.Extra


type alias Point =
    ( Int, Int )


type alias Board =
    Set Point


type Msg
    = Toggle Point
    | Step


init : Board
init =
    Set.empty


step : Board -> Board
step board =
    Set.union (neighborsAlive 3 board) (Set.intersect board (neighborsAlive 2 board))


neighborsAlive : Int -> Board -> Board
neighborsAlive n board =
    Set.toList board
        |> List.concatMap neighbors
        |> List.sort
        |> List.Extra.group
        |> List.filter (List.length >> (==) n)
        |> List.map (List.head >> Maybe.withDefault ( 0, 0 ))
        |> Set.fromList


neighbors : Point -> List Point
neighbors p =
    [ ( -1, -1 )
    , ( -1, 0 )
    , ( -1, 1 )
    , ( 0, -1 )
    , ( 0, 1 )
    , ( 1, -1 )
    , ( 1, 0 )
    , ( 1, 1 )
    ]
        |> List.map (addPoint p)


addPoint : Point -> Point -> Point
addPoint p q =
    case ( p, q ) of
        ( ( p1, p2 ), ( q1, q2 ) ) ->
            ( p1 + q1, p2 + q2 )



--== UPDATE ==--


update : Msg -> Board -> ( Board, Cmd Msg )
update msg board =
    case msg of
        Toggle cell ->
            (if Set.member cell board then
                Set.remove cell board
             else
                Set.insert cell board
            )
                ! []

        Step ->
            step board
                ! []
