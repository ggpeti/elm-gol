module Weather exposing (..)

import Geolocation
import Http
import Json.Decode
import Json.Helpers exposing ((:=))
import Json.Decode.Extra exposing ((|:))
import Task


type Msg
    = WeatherAPIResponse (Result Http.Error Weather)
    | Located Geolocation.Location
    | Nop


type alias Model =
    { weather : Weather
    , location : Maybe Geolocation.Location
    }


init : Model
init =
    { weather = initWeather
    , location = Nothing
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        WeatherAPIResponse (Ok newWeather) ->
            { model | weather = newWeather } ! []

        Located location ->
            { model | location = Just location } ! [ fetchWeather location ]

        _ ->
            model ! []


requestLocation : Cmd Msg
requestLocation =
    Task.attempt receiveLocation Geolocation.now


receiveLocation : Result Geolocation.Error Geolocation.Location -> Msg
receiveLocation result =
    case result of
        Ok location ->
            Located location

        Err error ->
            Nop


fetchWeather : Geolocation.Location -> Cmd Msg
fetchWeather location =
    Http.send WeatherAPIResponse <|
        Http.get
            ("https://api.openweathermap.org/data/2.5/weather?appid=8db45343f2b710bc45406dee9e82267d&lat=" ++ (toString location.latitude) ++ "&lon=" ++ (toString location.longitude))
            decodeWeather


type alias Weather =
    { coord : WeatherCoord
    , weather : List WeatherWeather
    , base : String
    , main : WeatherMain
    , visibility : Maybe Int
    , wind : WeatherWind
    , clouds : WeatherClouds
    , dt : Int
    , sys : WeatherSys
    , id : Int
    , name : String
    , cod : Int
    }


initWeather : Weather
initWeather =
    { coord =
        { lon = 0
        , lat = 0
        }
    , weather =
        [ { id = 0
          , main = ""
          , description = "fetching weather"
          , icon = ""
          }
        ]
    , base = ""
    , main =
        { temp = 273.15
        , pressure = 0
        , humidity = 0
        , temp_min = 0
        , temp_max = 0
        }
    , visibility = Just 0
    , wind =
        { speed = 0
        , deg = Just 0
        }
    , clouds =
        { all = 0
        }
    , dt = 0
    , sys =
        { sysType = 0
        , id = 0
        , message = 0
        , country = ""
        , sunrise = 0
        , sunset = 0
        }
    , id = 0
    , name = "..."
    , cod = 0
    }


type alias WeatherWeather =
    { id : Int
    , main : String
    , description : String
    , icon : String
    }


type alias WeatherCoord =
    { lon : Float
    , lat : Float
    }


type alias WeatherMain =
    { temp : Float
    , pressure : Int
    , humidity : Int
    , temp_min : Float
    , temp_max : Float
    }


type alias WeatherWind =
    { speed : Float
    , deg : Maybe Int
    }


type alias WeatherClouds =
    { all : Int
    }


type alias WeatherSys =
    { sysType : Int
    , id : Int
    , message : Float
    , country : String
    , sunrise : Int
    , sunset : Int
    }


decodeWeather : Json.Decode.Decoder Weather
decodeWeather =
    Json.Decode.succeed Weather
        |: ("coord" := decodeWeatherCoord)
        |: ("weather" := Json.Decode.list decodeWeatherWeather)
        |: ("base" := Json.Decode.string)
        |: ("main" := decodeWeatherMain)
        |: (Json.Decode.maybe ("visibility" := Json.Decode.int))
        |: ("wind" := decodeWeatherWind)
        |: ("clouds" := decodeWeatherClouds)
        |: ("dt" := Json.Decode.int)
        |: ("sys" := decodeWeatherSys)
        |: ("id" := Json.Decode.int)
        |: ("name" := Json.Decode.string)
        |: ("cod" := Json.Decode.int)


decodeWeatherWeather : Json.Decode.Decoder WeatherWeather
decodeWeatherWeather =
    Json.Decode.succeed WeatherWeather
        |: ("id" := Json.Decode.int)
        |: ("main" := Json.Decode.string)
        |: ("description" := Json.Decode.string)
        |: ("icon" := Json.Decode.string)


decodeWeatherCoord : Json.Decode.Decoder WeatherCoord
decodeWeatherCoord =
    Json.Decode.succeed WeatherCoord
        |: ("lon" := Json.Decode.float)
        |: ("lat" := Json.Decode.float)


decodeWeatherMain : Json.Decode.Decoder WeatherMain
decodeWeatherMain =
    Json.Decode.succeed WeatherMain
        |: ("temp" := Json.Decode.float)
        |: ("pressure" := Json.Decode.int)
        |: ("humidity" := Json.Decode.int)
        |: ("temp_min" := Json.Decode.float)
        |: ("temp_max" := Json.Decode.float)


decodeWeatherWind : Json.Decode.Decoder WeatherWind
decodeWeatherWind =
    Json.Decode.succeed WeatherWind
        |: ("speed" := Json.Decode.float)
        |: (Json.Decode.maybe ("deg" := Json.Decode.int))


decodeWeatherClouds : Json.Decode.Decoder WeatherClouds
decodeWeatherClouds =
    Json.Decode.succeed WeatherClouds
        |: ("all" := Json.Decode.int)


decodeWeatherSys : Json.Decode.Decoder WeatherSys
decodeWeatherSys =
    Json.Decode.succeed WeatherSys
        |: ("type" := Json.Decode.int)
        |: ("id" := Json.Decode.int)
        |: ("message" := Json.Decode.float)
        |: ("country" := Json.Decode.string)
        |: ("sunrise" := Json.Decode.int)
        |: ("sunset" := Json.Decode.int)
