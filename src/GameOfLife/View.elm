module GameOfLife.View exposing (..)

import GameOfLife exposing (..)
import Bootstrap.Button as Button
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Set


view : Int -> Board -> Html Msg
view boardSize board =
    div []
        [ viewIntro
        , viewControls
        , viewBoard boardSize board
        ]


viewIntro : Html Msg
viewIntro =
    div [ style [ ( "color", "white" ) ] ]
        [ text "This is a Conway's Game of Life simulator. Try it out!" ]


viewControls : Html Msg
viewControls =
    div []
        [ Button.button [ Button.onClick Step ] [ text "Step" ] ]


viewBoard : Int -> Board -> Html Msg
viewBoard size board =
    div
        [ style
            [ ( "height", toString (size * 41 + 1) ++ "px" )
            , ( "width", toString (size * 41 + 1) ++ "px" )
            , ( "position", "relative" )
            , ( "border", "1px solid black" )
            , ( "z-index", "1" )
            ]
        ]
    <|
        List.map (\cell -> viewCell size (size * 20) (cellColor board cell) cell) <|
            List.concatMap (\row -> (List.range -20 20) |> List.map ((,) row)) <|
                List.range -20 20


cellColor : Board -> Point -> String
cellColor board cell =
    if Set.member cell board then
        "black"
    else
        "white"


viewCell : Int -> Int -> String -> Point -> Html Msg
viewCell size offset color ( x, y ) =
    div
        [ style
            [ ( "height", toString size ++ "px" )
            , ( "width", toString size ++ "px" )
            , ( "position", "absolute" )
            , ( "left", toString (x * size + offset) ++ "px" )
            , ( "top", toString (y * size + offset) ++ "px" )
            , ( "background-color", color )
            , ( "border", "0.5px solid lightgray" )
            ]
        , onClick (Toggle ( x, y ))
        ]
        []
